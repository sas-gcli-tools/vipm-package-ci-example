# SAS CI Demo for building VIPM packages

This project is a demo to show how to setup CI/CD in LabVIEW to build VIPM Packages. It is intended to go with the [SAS-GCL-Tools](https://sas-gcli-tools.gitlab.io). It supposed to be an example of how to use the tools in a more realistic scenario. 

The intent is that someone could fork this project and following the instructions [here](https://sas-gcli-tools.gitlab.io/#setting-up-a-gitlab-runner) set up a GitLab Runner and by changing a few variables in the .yaml file, you could have CI up and running for your project.

## Table of contents
[TOC]

## Getting started

### Creating your own copy of this template
Start a new project in GitLab and select Import and then from URL and paste the following: https://gitlab.com/sas-gcli-tools/vipm-packag-ci-example.git. Check all the other settings and you should be good to go.


### Get a runner setup
- Setup a Runner folowing [these instructions](https://sas-gcli-tools.gitlab.io/#setting-up-a-gitlab-runner).  **NOTE**: This project is in LabVIEW 2020x64. If your runner uses LV 2020x64 then you shouldn't have to change any variables in the vars.yml file. Do check the BUILD_MACHINE_TAG variable and make sure that matches your tag on your runner. If you are using a newer version of LabVIEW or 32 bit LabVIEW, then you should adjust the variables accordingly. Also make sure you assign the runner to the forked project.
- Download the vipc from this repository and apply it to your runner. **NOTE**: Technically (if you have VIPM Pro) you really only need  g-cli, the vipc applier, and the clearlvcache tool. If you have that much then the CI job should install the rest from the vipc in the project, but those tools are needed to get the CI job to run initially.
- Make sure the runner is assigned to your project and shows up in the CI/CD Settings page.
- Check out the note below about VIPM Pro and if you don't have it, disable the VIPC application following the instructions there.

### Verify that the tools run locally

- Clone the repostory to obtain a local copy.
- Apply the vipc. Doing this manually does not require VIPM Pro.
- The `build.sh` script is the script that will get run on the CI Runner. By putting the commands in a shell script we can run it locally to make sure it works and to aid in troubleshooting. Because it loads environment variables from a yaml file (which we can import in the CI yml file), this helps to recreate the CI environment as close as we can.
- Open GitBash and run `./build.sh 9999`. The 9999 is the build number - when running the build script, you have to provide a build number (the first 3 semantic version values are specified in the vars.yml file). When running the CI pipeline in GitLab this number is supplied automatically.
- The script goes through the following steps:
    - Clears the LabVIEW cache.
    - Applies the VIPC file if needed.
    - Runs VITester (acts a placeholder if no tests are found)
    - Builds the VIPM Package
- You should see the following output: 
    - This script will run VI Tester and then build the executable and the run the package build specification to build an NI package.
    - The script should create a UnitTestReport.xml. It will be relatively empty because there aren't any tests yet in the project. It is just a placeholder. If you add VI Tester tests in the future, they will get run here and the results will show up in this file.
    - There should now be a builds directory with your built package

### Making changes and running the CI job on your runner

- Make a change to the project.
- Commit your Changes
- Optional but recommended especially if working in a parralel with others: Run `build.sh` locally to verify before pushing.
- Push your Changes
- If you have everything set up correctly, the CI should run automatically on the server
- Goto GitLab.com and open your project. Go to Build->Pipelines on the right hand menu and you should see a new pipeline running. You can click on the individual job to watch the status.
- If everything works, when the pipeline is finished, if you go back to the pipelines page, you should see a set of artifacts that includes the package, as well as the UnitTestReport.
- If you go to https://your_username_or_groupname.gitlab.io/project_name you will see the antidoc documentation. For example for this original project the docs are at https://sas-gcli-tools.gitlab.io/vipm-package-ci-example


## Template details

### CI stages and jobs
This project has 3 stages and 3 jobs defined in the .gitlab-ci.yml file. At the top we import a vars.yml file to define a bunch of environment variables for the project. This is designed so that you can reuse this file in other projects and simply change a few variables and it should work.

Here are the stages and jobs.
* build stage
  * build_pkg_exe - This job runs the build.sh shell script. Stores the build output and unit test results as artifacts. Runs on every commit. 
* upload stage
  * upload_job - This job uploads the VIPM package to the GitLab Package Repository. Only runs on tagged commits where the tag matches the $VERSION environment variable.
* release stage
  * release_job - This job creates the release in GitLab and links to the package in the package repository. Only runs on tagged commits where the tag matches the $VERSION environment variable.

### Shell scripts
The shell script is an attempt to replicate the CI process locally for debugging or verifying it passes before pushing. In order to mimic the build server environment as closely as possible, the script load environment variables from a vars.yml

Here is a brief decscription of what the shell script does.

* build.sh
  * Loads environment variables from vars.yml file
  * Clears the LabVIEW cache
  * Applies VIPC file
  * Runs the unit tests
  * Builds the Exe 
  * Builds the Package


### Note About VIPM Pro

This project expects that you have VIPM Pro. It uses the VIPM API to apply a vipc file using vipc g-cli tool and that requires the Pro version. I know there was talk of VIPM opening up some of these features in the free version in the future, so at some point this may not be a problem. For now, if you don't have VIPM Pro installed then you'll need to go into build.sh and comment out the line that applies the vipc file.

It line you are looking for should look like this:

```bash
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" vipc -- "$VIPC_PATH" -t "$VIPC_TIMEOUT" -v "$VIPC_LV_VERSION"
```

Just put a `#` in front of that line to comment it out. Save the file and check it in. 

**NOTE** That does mean if you ever make any changes to the VIPC, you'll have to manually apply them.

