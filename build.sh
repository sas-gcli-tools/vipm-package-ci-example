# use this to build locally - runs through the whole test_build_buildpkg process.
# pass in the bulid number - needs to be supplied since normally supplied by CI pipeline.

# do this before setting -eou pipefail to avoid unbound variable error.
if [[ -z ${1} ]]; then
   echo "Please supply a build number"
   exit 1
fi


set -eou pipefail # needed so script exits on errors.
SECONDS=0


# This line reads in all the variables in the gitlab CI file and creates the appropriate env variables.
# This allows us to define the vars once in the yml file
# and then run the build process locally using the same vars
# see https://unix.stackexchange.com/questions/539009/export-environment-variables-parsed-from-yaml-text-file
. <(sed -nr '/variables:/,$ s/  ([A-Z_]+): (.*)/\1=\2/ p' vars.yml)
# We set the full version here to whatever is passed in
# This is done after we read in the yaml file so we overwrite whatever was there
FULL_VERSION=$VERSION.$1
echo "Pipeline for Version $FULL_VERSION"
# clean up any remnants from previous runs.
# will all pass due to force -f
# not necessarily needed on build machine, but nice when running locally
# particularly for Unit test report as lunit just creates a new copy everytime
rm -f "$UNIT_TEST_REPORT_PATH"
rm -rf "$PKG_BUILD_DIR"
#

g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" clearlvcache # for G-CLI 3.0 and later can replace with ClearCache
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" vipc -- "$VIPC_PATH" -t "$VIPC_TIMEOUT" -v "$VIPC_LV_VERSION"
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" --kill "$UNIT_TEST_TOOL" -- -r "$UNIT_TEST_REPORT_PATH" "$LVPROJ_PATH"
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" vipb -- -b "$VIPB_PATH" -v "$FULL_VERSION" -rn "$RELEASE_NOTES_PATH" -t "$VIPB_TIMEOUT"
echo "Total Script Time: $SECONDS"

